# containers

[![pipeline status](https://git.coop/meet/containers/badges/master/pipeline.svg)](https://git.coop/meet/containers/-/commits/master)

The Docker containers which are automatically built from this repo, using GitLab CI, are designed for use by GitLab CI on [git.coop](https://git.coop/).

## BBB

This image is designed to be used by GitLab CI for running Ansible playbooks to test [BigBlueButton](https://bigbluebutton.org/) installs.

The `registry.git.coop/meet/containers/images/bbb` image is [Ubuntu](https://hub.docker.com/_/ubuntu) Xenial plus [Ansible 2.9.9](https://github.com/ansible/ansible/blob/stable-2.9/changelogs/CHANGELOG-v2.9.rst#release-summary), `molcule` and a number of other packages.

## Versioning

Please increment tags in the [.gitlab-ci.yml](./.gitlab-ci.yml) when making changes. We aim to follow [semver](https://semver.org/). This determines the docker tag available in the [container registry](https://git.coop/meet/containers/container_registry).
